
function table() {

  //alert("soy lucifer");
  $("#example1").DataTable({
    dom: 'Bfrtip',
    
    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
  }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
  });
  $('#example tbody').on('click', 'tr', function () {
    $(this).toggleClass('selected');
  });

  $('#button').click(function () {
    alert(table.rows('.selected').data().length + ' row(s) selected');
  });



};
