import { Component, OnInit, TemplateRef, ViewChild, ElementRef, Renderer2, Input } from '@angular/core';
import { LayoutService } from 'angular-admin-lte';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { faBars, faHome, faUser, faFirstAid, faInfo, faCaretDown,faUserAstronaut, faBell, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import {faMap} from '@fortawesome/free-regular-svg-icons'
import { PlaneListComponent } from './plane-list/plane-list.component';
import { TreeComponent, TreeNode, TreeModel, TREE_ACTIONS, KEYS, IActionMapping, ITreeOptions } from 'angular-tree-component';
import { trigger, state, style, animate, transition, } from '@angular/animations';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PlanosWithChildren } from './planoswithchildren';
import {Observable} from 'rxjs';




@Component({
  selector: 'app-planos',
  templateUrl: './planos.component.html',
  providers: [{ provide: BsDropdownConfig, useValue: { isAnimated: true, autoClose: true } }],
  styleUrls: ['./planos.component.css'],
  animations: [

    trigger('openClose', [
      state('true',

        style({
          transform: 'translateX(0%)'
        }),

      ),
      state(
        'false',

        style({
          transform: 'translateX(-100%)'

        })

      ),
      transition('false <=> true', animate('500ms'))


    ])
  ]


})
export class PlanosComponent implements OnInit {
  public customLayout: boolean;
  fondo: string = "blanco.png";
  modalRef: BsModalRef;

//icons
  faBars = faBars;
  faMap = faMap;
  faHome = faHome;
  faUser = faUser;
  faFirstAid = faFirstAid;
  faInfo = faInfo;
  faCaretDown = faCaretDown;
  faUserAstronaut = faUserAstronaut;
  faBell = faBell;
  faInfoCircle = faInfoCircle;

  // faSitemap = faSitemap;
  sidebarhide: boolean = true;


  @ViewChild("principal")
  private principal: ElementRef;
  @ViewChild(PlaneListComponent)
  private planeList: PlaneListComponent;
  @ViewChild(SidebarComponent)
  private sideBar: SidebarComponent;

  alertas: any[]=[];


  constructor(
    private layoutService: LayoutService, private modalService: BsModalService, private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.layoutService.isCustomLayout.subscribe((value: boolean) => {
      this.customLayout = value;
    });
  }

  mostrarSidebar(): void {
    this.sidebarhide = !this.sidebarhide;
    console.log("sidebarhide",this.sidebarhide);

    //this.renderer.removeClass(this.principal.nativeElement,'col-md-10 col-sm-10 col-xs-12');
    if (!this.sidebarhide) {

       //this.renderer.removeClass(this.principal.nativeElement, 'princi'); //eliminar class

       this.renderer.removeClass(this.principal.nativeElement, 'toggeled-sidebar'); //tamaño pantallas pequeñas

    } else {


      this.renderer.addClass(this.principal.nativeElement, 'toggeled-sidebar'); //tamaño pantallas pequeñas



    }
    console.log("sidebarhide:", this.sidebarhide);


  }


  //PopUp ayuda-leyenda de colores
  openModal(template1: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template1, { class: 'modal-sm' });
  }
  //PopUp ayuda-Acerca de Argos
  openModal2(template2: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template2, { class: 'modal-sm' });
  }
  //PopUp ayuda-Acerca de Isis
  openModal3(template3: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template3, { class: '' });
  }


  onChangePlano(event) {
    console.log("onChangePlano de planos.component.ts:", event);

    this.planeList.changePlano(event);

  }

  onChangePlanoAlertas(event) {
    console.log("onChangePlanoAlertas de planos.component.ts:", event);

    this.alertas=event.alertas;
    console.log("onChangePlanoAlertas alertas ",this.alertas);
    this.planeList.changePlano(event);

  }




  onAnularAlarmas(event){
    console.log("onAnularAlarmas de planos.componet.ts",event);

    this.planeList.anularAlarmas(event);
    this.sideBar.anularAlarmas(event);
  }


  OnMensajeAlert(event){
    console.log("onMensajeAlert de planos.componet.ts",event);

  }








}
