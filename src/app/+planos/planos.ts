import { RecursosPlano } from "./recursosPlano";


export class Planos {
  PKid: number;
  idPlanoSup: number;
  estadoConexion: number;
  idPlanoIzda: number;
  idPlanoDcha: number;
  posicion: number;
  recursosPlano: RecursosPlano[][];
  recursoPlano: object;
  fichero: string;
  tipoRecurso: number;
  idPlanoInf: number;
  nombreFichero: string;
  estadoOperativo: number;
  nombreDinamico: string;
  idPlanoPadre: number;
  path:string;
  FKdominio: string;
  descripcion:string;
  nombre:string;
  usuariosConPermiso: number[];
  fechaActFichero: number;
}
