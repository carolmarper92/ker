import { Employee } from "./employee";

export class EmployeeWithChildren {
  id: number;
  firstName: string;
  lastName: string;
  emailId: string;
  active: boolean;
  idParent: number;
  name: string;
  isExpanded: boolean=true;
//  children: Employee[]=[];
}
