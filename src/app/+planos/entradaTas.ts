export class EntradaTas {
    tipoTarjeta: number;
    FKdominio: string;
    tipoEntradaTas: number;
    usi: string;
    tipoRecurso: number;
    tipoMultiestado: number;
    mascara: number;
    codPrioridad: number;
    nombre: string;
    FKtarjeta: number;
    idGrupo: number;
    tvoAsociado:number;
    estadoAlarma:number;
    estadoProg:number;
    PKid:number;
    nombreDinamico:string;
    conexionOneWire:boolean;
    texto:string;
    estadoConexion:number;
    estadoOperativo:number;
    gCamaras:string;
    tipo:number;
    grupo:number;
    conexion:number;
    idSirena:number;
    estado:number;
    estadoOperacional:number;
    idAutorizador:number;

}