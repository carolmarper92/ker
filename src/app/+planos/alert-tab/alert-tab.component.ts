import { Component, OnInit, ViewChild, ElementRef, TemplateRef, Renderer2, HostBinding, Input } from "@angular/core";
import { trigger, state, style, animate, transition } from '@angular/animations';
import { PlanosWithChildren } from "../planoswithchildren";
import {Observable} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';

interface Opciones {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-alert-tab',
  templateUrl: './alert-tab.component.html',
  styleUrls: ['./alert-tab.component.css'],
  animations: [

    trigger('openClose', [
      state('true',

        style({
          transform: 'translateY(0%)'
        }),
      ),
      state(
        'false',

        style({

          transform: 'translateY(-100%)',

        })

      ),
      transition('false <=> true', animate('500ms'))


    ])
  ]

})
export class AlertTabComponent implements OnInit {

  icon: string = "glyphicon glyphicon-chevron-up";
  tabhide: boolean = true;

  selectedOpciones: string;
  opciones: Opciones[] = [
    {value: 'mantenimiento', viewValue: 'Mantenimiento'},
    {value: 'seguridad', viewValue: 'Seguridad'},
    {value: 'limpieza', viewValue: 'Limpieza'}
  ];



  @ViewChild('tab')
  private tab: ElementRef;
  form: FormGroup;

/*
  @Input('planos')
  planos: Observable<PlanosWithChildren[]>;
*/

  @Input('alertas')
  alertas: any;
  // public subscribers: any = {};

  constructor(private renderer2: Renderer2, private fb: FormBuilder) { }

  ngOnInit() {
    console.log("ngOninit AlertTabComponent");
    this.form = this.fb.group({
      acciones: [''],
    });


  }

  ngOnDestroy(): void{
    console.log("AlertTabComponent ngOnDestroy");
  }

  datashow(): void {
    this.tabhide = !this.tabhide;

    if (!this.tabhide) {
      this.icon = "glyphicon glyphicon-chevron-down";
      //this.tab.nativeElement.classList.remove('footer');

    } else {
      this.icon = "glyphicon glyphicon-chevron-up";
      //this.tab.nativeElement.classList.add('footer');

    }
    console.log("tabhide:", this.tabhide);
  }
}
