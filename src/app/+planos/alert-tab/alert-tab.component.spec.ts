import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AlertTabComponent } from './alert-tab.component';

describe('AlertTabComponent', () => {
  let component: AlertTabComponent;
  let fixture: ComponentFixture<AlertTabComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
