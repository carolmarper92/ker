import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {Observable} from "rxjs/index";
import {Recurso} from "../recurso";

@Component({
  selector: 'app-btn-generic',
  templateUrl: './btn-generic.component.html',
  styleUrls: ['./btn-generic.component.css']
})
export class BtnGenericComponent  {

  public active: boolean;



  tipoRecurso:number;
  PKid: number;
  estadoConexion: number;
  destino:number;
  colorFondo: number;
  nombreFuenteLetra:string;
  filaIni: number;
  numfilas:number;
  texto:string;
  estadoOperativo:number;
  nombreDinamico:string;
  numColumnas:number;
  usi:string;
  columnaIni:number;
  colorLetra:number;
  tamanioLetra: number;
  FKdominio: string;
  recursoPlano: Recurso[];
  estiloLetra: number;
  tipoVentana:number;
  nombre:string;


  public operation;


  constructor(public dialogRef: MatDialogRef<BtnGenericComponent>) {

  }


  ngOnInit() {
  }

  onAcceptButton(){
    this.operation = "ACCEPT";
    this.dialogRef.close();
  }

  onCancelButton(){
    this.operation = "CANCEL";
    this.dialogRef.close();
  }


}
