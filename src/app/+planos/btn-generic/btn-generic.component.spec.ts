import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BtnGenericComponent } from './btn-generic.component';

describe('BtnGenericComponent', () => {
  let component: BtnGenericComponent;
  let fixture: ComponentFixture<BtnGenericComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnGenericComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnGenericComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
