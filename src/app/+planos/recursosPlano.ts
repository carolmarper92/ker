import { Recurso } from "./recurso";

export class RecursosPlano {
  tipoRecurso:number; 
  PKid: number;
  estadoConexion: number;
  destino:number;
  colorFondo: number;
  nombreFuenteLetra:string;
  filaIni: number;
  numfilas:number;
  texto:string;
  estadoOperativo:number;
  nombreDinamico:string;
  numColumnas:number;
  usi:string;
  columnaIni:number;
  colorLetra:number;
  tamanioLetra: number;
  FKdominio: string;
  recursoPlano: Recurso[];
  estiloLetra: number;
  tipoVentana:number;
  nombre:string;
  backColor: string;

}
