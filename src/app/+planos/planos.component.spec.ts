import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PlanosComponent } from './planos.component';

describe('PlanosComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        PlanosComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(PlanosComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'angular6-springboot-client'`, () => {
    const fixture = TestBed.createComponent(PlanosComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('angular6-springboot-client');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(PlanosComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to angular6-springboot-client!');
  });
});
