import { Observable } from "rxjs";
import { PlanoService } from "../plano.service";
import { Employee } from "./../employee";
import { Component, OnInit, ViewChild, ElementRef, TemplateRef, Renderer2, HostListener, Input } from "@angular/core";
import { Router } from '@angular/router';
import { TreeComponent, TreeNode, TreeModel, TREE_ACTIONS, KEYS, IActionMapping, ITreeOptions } from 'angular-tree-component';
import { EmployeeWithChildren } from '../employeewithchildren';
import { promise, Key } from 'selenium-webdriver';
import _ from "lodash";
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import $ from 'jquery';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Planos } from '../planos';
import { RecursosPlano } from '../recursosPlano';
import {EntradaTas} from '../entradaTas';
import { DomSanitizer } from '@angular/platform-browser';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {PlanoDialogComponent} from "../plano-dialog/plano-dialog.component";
import {BtnGenericComponent} from "../btn-generic/btn-generic.component";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Recurso} from "../recurso";
import {MessageService} from 'primeng/api';
import { ToastrService } from "ngx-toastr";




@Component({
  selector: "app-plane-list",
  templateUrl: "./plane-list.component.html",
  styleUrls: ["./plane-list.component.scss"],
  providers:[MessageService]
})

export class PlaneListComponent implements OnInit {


  planojson: Observable<any>;
  tasjson: Observable<any>;
  nodes: any[];
  fondo: string;
  planos: Planos;
  entradaTAs: EntradaTas;
  //planos2: RecursosPlano[];
  planos2 = new Array<RecursosPlano>();
  entradaTas2= new Array<EntradaTas>();
  prueba2: any;


  @ViewChild('tree')
  private tree: TreeComponent;

  @ViewChild('central')
  private central: ElementRef;

  @ViewChild('imgprincipal')
  private imgPrincipal: ElementRef;

  @Input('parentDiv')
  private parentDiv: HTMLElement;


  imagePath: any;
  imagePathIcon: any;
  nombrePlano:any;
  escalado: any;

  public imgWidth = 1000;
  public imgHeight = 1000;




  private serverUrl = 'http://127.0.0.1:8080/socket'
  // private serverUrl = 'http://127.0.0.1:4200/socket'
  // private serverUrl = 'http://127.0.0.1:8888/socket'
  private stompClient;
  options: ITreeOptions = {
  }



  changezindex(nav:HTMLElement){

    console.log("navvvvvv "+nav);
    let zIndex=nav.style.zIndex;
    console.log("zIndexxxx nav "+zIndex);
    if (zIndex=="1"){
      nav.style.zIndex="1000";
      this.renderer.removeAttribute(nav, 'z-index');
      this.renderer.setProperty(nav, 'z-index', '' + 1000);
    }else{
      nav.style.zIndex="1";
      this.renderer.removeAttribute(nav, 'z-index');
      this.renderer.setProperty(nav, 'z-index', '' + 1);
    }
  }
  constructor(private PlanoService: PlanoService,
    private router: Router, private modalService: BsModalService, private _sanitizer: DomSanitizer, private renderer: Renderer2, private el: ElementRef,
              private dialog: MatDialog, private messageService: MessageService,private toastr: ToastrService) { }





  ngOnInit() {

    // this.reloadDataJson();
    this.initializeWebSocketConnection();
  }
//mostrar alertas

showSuccess() {
  this.toastr.warning('Caja Fuerte', 'Alarma', {
    closeButton: true,
    newestOnTop: true,
    timeOut: 5000,
    extendedTimeOut: 1000,
    
    
  });
}

  reloadDataJson() {
    console.log("PATH IMG", this.imagePath);
    // this.imagePath = 'assets/img/blanco.png';
    console.log("PATH IMG", this.imagePath);

    this.planojson = this.PlanoService.getPlanoJson();
    this.planojson.subscribe(
      (data: Planos[]) => {
        console.log("los datos", data);
        console.log("el data[8452].recursosPlano[0] ", data[8452].recursosPlano[0]);
        console.log("el data[8452].recursosPlano[0][5734] ", data[8452].recursosPlano[0][5734]);
        console.log("el data[8452].usuariosConPermiso ", data[8452].usuariosConPermiso);
        console.log("el data[8452].usuariosConPermiso[1092] ", data[8452].usuariosConPermiso[1092]);


        Object.keys(data).map(key => {
          console.log("la key", key);
          console.log("los datos de ", data[key]);
          console.log("el recursoPlano ", data[key].recursoPlano);
          console.log("el pkid ", data[key].pkid);
          console.log("el PKid ", data[key].PKid);
          if (data[key].recursosPlano != null) {
            Object.keys(data[key].recursosPlano).map(key2 => console.log("el recursosPlano11111 ", data[key].recursosPlano[key2]));
          }
        });
        // Object.keys(data).map(key => console.log("los datos de ",data[key]));

        console.table(data);


        // console.log("json generado", JSON.stringify(data, null, 2));
        // this.tree.treeModel.update();
      },
      error => console.log(error));





  }


  onUpdateData(treeComponent: TreeComponent, $event) {
    /*console.log ("onUpdateData", treeComponent, $event);
    treeComponent.treeModel.expandAll();*/
  }

  initializeWebSocketConnection() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/chat", (message) => {
        if (message.body) {
          $(".chat").append("<div class='message'>" + message.body + "</div>")
          console.log(message.body);
          let jsonData = JSON.parse(message.body);
          $("#node_" + jsonData.id).attr('class', jsonData.alerta);
        }
      });
    });
  }

  sendMessage(message) {
    this.stompClient.send("/app/send/message", {}, message);
    $('#input').val('');
    this.central.nativeElement.classList.remove('mostrar');
    this.central.nativeElement.classList.add('ocultar');
  }


  openPlano(event) {
    this.central.nativeElement.classList.remove('ocultar');
    this.central.nativeElement.classList.add('mostrar');
    this.fondo = "plano.jpeg";
    this.fondo = "P1.JPG";


    console.log("evento:", event);
  }
  public anularAlarmas(event) {
    console.log("anularAlarmas de plane-list.component.ts:", event);
    console.log("anularAlarmas de plane-list.component.ts---ANTES-----FONDO>:", this.fondo);
    this.planos2.map(recursosPlano => {
      recursosPlano.backColor="green";
      return recursosPlano;
    });
  }
  public changePlano(event) {
    console.log("changePlano de plane-list.component.ts:", event);
    console.log("changePlano de plane-list.component.ts---ANTES-----FONDO>:", this.fondo);

    if (event.planoUrl!=null){
      this.fondo = event.planoUrl.nombreFichero;
      this.nombrePlano=event.planoUrl.nombre;
    }
    this.planos = event.planoUrl;

    this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
      + this.planos.fichero);



    console.log("LO QUE TRAE", this.planos);




    //Prueba 1 mostrar recursosPlano
    for (let elemento in this.planos.recursosPlano[27]) {
      console.log("LO QUE TRAE-recursosplano 27", elemento);

      this.prueba2 = this.planos.recursosPlano[27][elemento];
      console.log("PRUEBA2", this.prueba2);

    }

    //prueba 2 mostrar recursosPlano
    Object.keys(this.planos.recursosPlano).map(key => {
      console.log("SEGUNDA-la key", key);
      console.log("los datos de ", this.planos.recursosPlano[key]);
      //console.log("el recursoPlano ", this.planos[key].recursoPlano);
      //console.log("el pkid ", this.planos[key].pkid);
      //console.log("el PKid ", this.planos[key].PKid);

      Object.keys(this.planos.recursosPlano[key]).map(key2 => {
        this.planos2 = this.planos.recursosPlano[key][key2]
        console.log("el recursosPlano222222 ", this.planos.recursosPlano[key][key2])


      });
      console.log("PLANOS2", this.planos2);


    });

    // prueba 3 mostrar recursosPlano
    Object.keys(this.planos.recursosPlano).map(key => {
      console.log("TERCERA-la key", key);
      console.log("tercera-los datos de ", this.planos.recursosPlano[key]);
      //console.log("el recursoPlano ", this.planos[key].recursoPlano);
      //console.log("el pkid ", this.planos[key].pkid);
      //console.log("el PKid ", this.planos[key].PKid);

      this.planos2 = Object.keys(this.planos.recursosPlano[key]).map(key2 => {
        this.planos.recursosPlano[key][key2];
        console.log("el recursosPlano333333 ", this.planos.recursosPlano[key][key2])
        return this.planos.recursosPlano[key][key2];

      });
      console.log("PLANOS3", this.planos2);


    });

    //prueba 4 mostrar recursosPlanos (Definitiva)



    Object.keys(this.planos.recursosPlano).map(key => {
      console.log("CUARTA-la key", key);
      console.log("cuarta-los datos de ", this.planos.recursosPlano[key]);
      this.imagePathIcon = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'+ this.planos.PKid);







      Object.keys(this.planos.recursosPlano[key]).map(key2 => {
        this.planos.recursosPlano[key][key2];
        console.log("el recursosPlano44444 ", this.planos.recursosPlano[key][key2]);

        this.planos2.push(this.planos.recursosPlano[key][key2]);
        console.log("aaaaa<<<<",this.planos.recursosPlano[key][key2].backColor,this.planos.recursosPlano[key][key2].PKid);

      }
      );
      console.log("444444444444444444");
      console.log("000000000000000000 ");
      console.log("this.imagePathIcon",this.imagePathIcon);

      //console.log("tmp11111", tmp);
      console.log("PLANOS2", this.planos2);


    });



    // console.log("LO QUE TRAE",this.planos.recursosPlano[27][].PKid);
    console.log("LO QUE TRAE", this.planos.PKid);
    console.log("changePlano de employee-list.component.ts-------->:", this.fondo);
    console.log("LO QUE TRAE-CUARTA", this.planos2);
  }


  onImageLoad() {
    // Do what you need in here
    console.log('imgPrincipal33333 height---' + this.imgPrincipal.nativeElement.offsetHeight);
    console.log('imgPrincipal3333 width---' + this.imgPrincipal.nativeElement.offsetWidth);
    this.renderer.removeAttribute(this.imgPrincipal.nativeElement, 'width');
    this.renderer.removeAttribute(this.imgPrincipal.nativeElement, 'height');
    console.log('imgPrincipal44444 height---' + this.imgPrincipal.nativeElement.offsetHeight);
    console.log('imgPrincipal44444 width---' + this.imgPrincipal.nativeElement.offsetWidth);


    console.log("this.planos.nombreFichero " + this.planos.nombreFichero);
    console.log("this.planos.fichero " + this.planos.fichero);

    let imgWidth = this.imgPrincipal.nativeElement.offsetWidth;
    let imgHeight = this.imgPrincipal.nativeElement.offsetHeight;

    this.renderer.setProperty(this.imgPrincipal.nativeElement, 'width', '' + imgWidth);
    this.renderer.setProperty(this.imgPrincipal.nativeElement, 'height', '' + imgHeight);

    console.log('imgPrincipal5555 height---' + this.imgPrincipal.nativeElement.offsetHeight);
    console.log('imgPrincipal5555 width---' + this.imgPrincipal.nativeElement.offsetWidth);

    console.log("screennnnnnnnnn dimmensions ", window.innerHeight, window.innerWidth);
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;

    console.log("imgPrincipallll", this.imgPrincipal);
    console.log("parentDiv1111", this.parentDiv);
    // console.log("parentDiv1111.width", this.parentDiv.offsetWidth);

    console.log("this.imgPrincipal", this.imgPrincipal);
    console.log("this.el", this.el);
    console.log("this.el.nativeElement", this.el.nativeElement);

    const hostElem = this.el.nativeElement;
    console.log("this.el.children", hostElem.children);
    console.log("this.el.parentnode", hostElem.parentNode);



    let parentWidth = this.el.nativeElement.parentNode.offsetWidth;
    let parentHeight = this.el.nativeElement.parentNode.offsetHeight;
    /*
        let parentX = this.el.nativeElement.offsetParent.offsetLeft;
        let parentY = this.el.nativeElement.offsetParent.offsetTop;
    */
        let parentX = this.el.nativeElement.parentNode.offsetLeft;
        let parentY = this.el.nativeElement.parentNode.offsetTop;


    console.log("parentWidth ", parentWidth, " parentHeight ", parentHeight, " parentX ", parentX, " parentY ", parentY);

    console.log("this.screenHeight ", this.screenHeight, "this.screenWidth", this.screenWidth);
    let escalaX = 1;
    let escalaY = 1;


    if ((parentX + imgWidth) > this.screenWidth) {
      console.log((parentX + imgWidth), ">", this.screenWidth);
      if (parentX < 20) {
        // if (Utils.isMobile()){
        //estamos en mobiles y el margen deberia ser 10 y por lo tanto reescalamos la x con un valor restando para que encaje
        escalaX = (this.screenWidth / (parentX + imgWidth)) - 0.05;
      } else {
        escalaX = (this.screenWidth / (parentX + imgWidth)) - 0.1;
      }
    } else {
      console.log((parentX + imgWidth), "<=", this.screenWidth);
      if (parentX < 20) {
        //estamos en mobiles y el margen deberia ser 10 y por lo tanto reescalamos la x con un valor restando para que encaje
        escalaX = (this.screenWidth / (parentX + imgWidth)) - 0.05;
      } else {
        escalaX = (this.screenWidth / (parentX + imgWidth)) - 0.05;
      }
    }

    if ((parentY + imgHeight) > this.screenHeight) {
      console.log((parentY + imgHeight), ">", this.screenHeight);
      if (parentX < 20) {
        //estamos en mobiles y el margen deberia ser 10 y por lo tanto reescalamos la y con el mismo valor que la x
        escalaY = escalaX;
      } else {
        //le restamos un 0.1 para que las imagenes grandes encaje probar con valores mas pequeños
        escalaY = (this.screenHeight / (parentY + imgHeight)) - 0.1;
      }
    } else {
      console.log((parentY + imgHeight), "<=", this.screenHeight);
      if (parentX < 20) {
        //estamos en mobiles y el margen deberia ser 10 y por lo tanto reescalamos la y con el mismo valor que la x
        escalaY = escalaX;
      } else {
        //le restamos un 0.1 para que las imagenes grandes encaje probar con valores mas pequeños
        escalaY = (this.screenHeight / (parentY + imgHeight)) - 0.1;
      }
    }

    console.log("escalaX ", escalaX, " escalaY ", escalaY);

    this.escalado = {
      'transform': 'scale(' + escalaX + ',' + escalaY + ')',
      'transform-origin': '0 0',
      // 'transform': 'scale(0.5)',
      'background-color': ''
    };
  }


  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    console.log("screennnnnnnnnn ", this.screenHeight, this.screenWidth);
    this.onImageLoad();
  }
  screenHeight: number;
  screenWidth: number;



  onEdit(classr) {
    console.log("estoy en onEdit ",classr);
    /*if(classr.operation==="ACCEPT") {
      this.edit.emit({
        id: classr.id,
        code: classr.code,
        description: classr.description,
        comment: classr.comment,
        active: classr.active,
        startPointOutputType: classr.startPointOutputType,

        address: classr.address,
        zipCode: classr.zipCode,
        city: classr.city,
        province: classr.province,
        country: classr.country,
        zone: classr.zone,
        latitude: classr.latitude,
        longitude: classr.longitude,

        directionDescription: classr.description,
        directionComment: classr.directionComment

      });
    }*/
  }

  openEditDialog(recursoPlano: RecursosPlano) {
    console.log("dentro de openEditDialogggggg1111 "+recursoPlano.PKid);
    let dialogRef: MatDialogRef<PlanoDialogComponent>;
    dialogRef = this.dialog.open(PlanoDialogComponent);

    dialogRef.componentInstance.tipoRecurso=recursoPlano.tipoRecurso;
    dialogRef.componentInstance.PKid = recursoPlano.PKid;
    dialogRef.componentInstance.estadoConexion = recursoPlano.estadoConexion;
    dialogRef.componentInstance.destino = recursoPlano.destino;
    dialogRef.componentInstance.colorFondo = recursoPlano.colorFondo;
    dialogRef.componentInstance.nombreFuenteLetra = recursoPlano.nombreFuenteLetra;
    dialogRef.componentInstance.filaIni = recursoPlano.filaIni;
    dialogRef.componentInstance.numfilas = recursoPlano.numfilas;
    dialogRef.componentInstance.texto = recursoPlano.texto;
    dialogRef.componentInstance.estadoOperativo = recursoPlano.estadoOperativo;
    dialogRef.componentInstance.nombreDinamico = recursoPlano.nombreDinamico;
    dialogRef.componentInstance.numColumnas = recursoPlano.numColumnas;
    dialogRef.componentInstance.usi = recursoPlano.usi;
    dialogRef.componentInstance.columnaIni = recursoPlano.columnaIni;
    dialogRef.componentInstance.colorLetra = recursoPlano.colorLetra;
    dialogRef.componentInstance.tamanioLetra = recursoPlano.tamanioLetra;
    dialogRef.componentInstance.FKdominio = recursoPlano.FKdominio;
    dialogRef.componentInstance.recursoPlano = recursoPlano.recursoPlano;
    dialogRef.componentInstance.estiloLetra = recursoPlano.estiloLetra;
    dialogRef.componentInstance.tipoVentana = recursoPlano.tipoVentana;
    dialogRef.componentInstance.nombre = recursoPlano.nombre;

    dialogRef.afterClosed().subscribe(res => this.onEdit(dialogRef.componentInstance));
  }
  openBtn(recursoPlano: RecursosPlano) {
    let dialogRef: MatDialogRef<BtnGenericComponent>;
    dialogRef = this.dialog.open(BtnGenericComponent);

    dialogRef.componentInstance.tipoRecurso=recursoPlano.tipoRecurso;
    dialogRef.componentInstance.PKid = recursoPlano.PKid;
    dialogRef.componentInstance.estadoConexion = recursoPlano.estadoConexion;
    dialogRef.componentInstance.texto = recursoPlano.texto;
    dialogRef.componentInstance.estadoOperativo = recursoPlano.estadoOperativo;
    dialogRef.componentInstance.nombreDinamico = recursoPlano.nombreDinamico;
    dialogRef.componentInstance.nombre = recursoPlano.nombre;

    dialogRef.afterClosed().subscribe(res => this.onEdit(dialogRef.componentInstance));
  }

  onCreate(classr) {
    console.log("estoy en onCreate ",classr);
    /*    if(classr.operation==="ACCEPT") {
          if (classr.code && classr.description) {
            if (!classr.comment) {
              classr.comment = '';
            }
            this.createStartPoint.emit(new StartPoint(
              classr.code,
              classr.description,
              classr.comment,
              classr.active,
              classr.address,
              classr.zipCode,
              classr.city,
              classr.province,
              classr.country,
              classr.zone,
              classr.latitude,
              classr.longitude,
              classr.startPointOutputType,
              classr.description,
              classr.directionDescription
            ));
          }
        }*/
  }

  openNewDialog() {
    let dialogRef: MatDialogRef<PlanoDialogComponent>;
    dialogRef = this.dialog.open(PlanoDialogComponent);

    // dialogRef.componentInstance.startPointOutputTypes=this.startPointOutputTypeElements;
    dialogRef.afterClosed().subscribe(res => this.onCreate(dialogRef.componentInstance));
  }


}
