
import {Observable, of} from 'rxjs';
import { PlanoService } from "../plano.service";
import { Employee } from "./../employee";
import { Component, OnInit, ViewChild, ElementRef, TemplateRef, EventEmitter, Input, Output } from "@angular/core";
import { Router } from '@angular/router';
import { TreeComponent, TreeNode, TreeModel, TREE_ACTIONS, KEYS, IActionMapping, ITreeOptions } from 'angular-tree-component';
import { EmployeeWithChildren } from '../employeewithchildren';
import { promise } from 'selenium-webdriver';
import _ from "lodash";
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';

import $ from 'jquery';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PlaneListComponent } from "../plane-list/plane-list.component";
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Planos } from "../planos";
import { PlanosWithChildren } from "../planoswithchildren";
import { interval } from 'rxjs';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],

})
export class SidebarComponent implements OnInit {

  employees: Observable<Employee[]>;
  employeesWithChildren: EmployeeWithChildren[];
  planos: Observable<Planos[]>;
  planosWithChildren: PlanosWithChildren[];

  nodes: any[];
  fondo: string = "blanco.png";
  public valueSidebar: boolean = true;
  selectedData: any;

  @Output() changePlanoAlertas: EventEmitter<any> = new EventEmitter(false);
  @Output() changePlano: EventEmitter<any> = new EventEmitter(false);
  @Output() mensajeAlerta: EventEmitter<any> = new EventEmitter(false);

  @ViewChild('tree')
  private tree: TreeComponent;

  @ViewChild(PlaneListComponent)
  private central: ElementRef;


  @ViewChild('sidebar')
  private sidebar: ElementRef;



  private serverUrl = 'http://127.0.0.1:8080/socket'
  // private serverUrl = 'http://127.0.0.1:4200/socket'
  // private serverUrl = 'http://127.0.0.1:8888/socket'
  private stompClient;
  options: ITreeOptions = {
  }

  private alertas: any[]=[];

  constructor(private PlanoService: PlanoService,
    private router: Router, private modalService: BsModalService) { }





  ngOnInit() {
    //this.reloadData2();
    this.reloadData2New();

    this.initializeWebSocketConnection();
    /*
        interval(10000).subscribe((val) => {
         console.log("caaaaaaaaaaaaaaaallleeeedddd");
         this.reloadData2Simu();
       });
    */
        setTimeout(()=>{
         console.log("caaaaaaaaaaaaaaaallleeeedddd");
         //this.reloadData2Simu();
         this.reloadData2SimuNew();

       },10000);

  }

  reloadData() {
    this.employees = this.PlanoService.getEmployeesList();
    this.employees.subscribe(
      (data: Employee[]) => {
        console.log("los datos", data);

        this.employeesWithChildren = data.map((employee: Employee) => {
          let employeeTmp: EmployeeWithChildren = new EmployeeWithChildren();
          employeeTmp.id = employee.id;
          employeeTmp.name = employee.firstName;
          employeeTmp.firstName = employee.firstName;
          employeeTmp.lastName = employee.lastName;
          employeeTmp.emailId = employee.emailId;
          employeeTmp.idParent = employee.idParent;
          return employeeTmp;
        });


        console.table(this.employeesWithChildren);

        this.nodes = this.transformToTree(this.employeesWithChildren);

        console.log("json generado", JSON.stringify(this.nodes, null, 2));
        // this.tree.treeModel.update();
      },
      error => console.log(error));
  }
  reloadData2() {
    this.planos = this.PlanoService.getPlanoJson();
    this.planos.subscribe(
      (data: Planos[]) => {
        console.log("los datos", data);

        this.planosWithChildren = Object.keys(data).map((key: string) => {
          let planoTmp: PlanosWithChildren = new PlanosWithChildren();
          planoTmp.FKdominio = data[key].FKdominio;
          planoTmp.PKid = data[key].PKid;
          planoTmp.idPlanoPadre = data[key].idPlanoPadre;
          planoTmp.nombre = data[key].nombre;
          planoTmp.nombreFichero = data[key].nombreFichero;
          planoTmp.recursosPlano = data[key].recursosPlano;
          planoTmp.fichero = data[key].fichero;



          return planoTmp;
        });


        console.table(this.planosWithChildren);

        this.nodes = this.transformToTree(this.planosWithChildren);

        console.log("json generado", JSON.stringify(this.nodes, null, 2));
        // this.tree.treeModel.update();
      },
      error => console.log(error));
  }
  reloadData2New() {
    this.planos = this.PlanoService.getPlanoJson();
    this.planos.subscribe(
      (data: any) => {
        console.log("reloadData2 los datos", data);

        this.planosWithChildren = data.plano.map((plane: Planos) => {
          console.log("reloadData2 plane ",plane);
          let planoTmp: PlanosWithChildren = new PlanosWithChildren();
          planoTmp.FKdominio = plane.FKdominio;
          planoTmp.PKid = plane.PKid;
          planoTmp.idPlanoPadre = plane.idPlanoPadre;
          planoTmp.nombre = plane.nombre;
          planoTmp.nombreFichero = plane.nombreFichero;
          planoTmp.recursosPlano = plane.recursosPlano;
          planoTmp.fichero = plane.fichero;



          return planoTmp;
        });

        console.table(this.planosWithChildren);

        this.nodes = this.transformToTree(this.planosWithChildren);

        console.log("json generado", JSON.stringify(this.nodes, null, 2));
        // this.tree.treeModel.update();
      },
      error => console.log(error));
  }
  reloadData2SimuNew() {
    let backColors: string[] = ["red", ""];
    let backColors2: string[] = ["green", "yellow", "red"];

    this.planos = this.PlanoService.getPlanoJson();
    this.planos.subscribe(

      (data: any) => {
        console.log("reloadData2SimuNew los datos", data);

        this.planosWithChildren = data.plano.map((plane: Planos) => {
          let planoTmp: PlanosWithChildren = new PlanosWithChildren();
          planoTmp.FKdominio = plane.FKdominio;
          planoTmp.PKid = plane.PKid;
          planoTmp.idPlanoPadre = plane.idPlanoPadre;
          planoTmp.nombre = plane.nombre;
          planoTmp.nombreFichero = plane.nombreFichero;
          planoTmp.recursosPlano = plane.recursosPlano;
          planoTmp.fichero = plane.fichero;
          let randomIndex = Math.floor(Math.random() * backColors.length);
          let randomColor = backColors[randomIndex];
          planoTmp.backColor = randomColor;

          Object.keys(planoTmp.recursosPlano).map(key => {
            console.log("sidebar", key);
            console.log("sidebar ", planoTmp.recursosPlano[key]);

            Object.keys(planoTmp.recursosPlano[key]).map(key2 => {
                planoTmp.recursosPlano[key][key2];
                console.log("sidebar ", planoTmp.recursosPlano[key][key2]);

                let randomIndex2 = Math.floor(Math.random() * backColors2.length);
                let randomColor2 = backColors2[randomIndex2];
                planoTmp.recursosPlano[key][key2].backColor = randomColor2;
              console.log("sidebar planoTmp.recursosPlano[key][key2].backColor ", planoTmp.recursosPlano[key][key2].backColor);
                if (planoTmp.recursosPlano[key][key2].backColor == "red") {
                  this.mensajeAlerta.emit(planoTmp);
                }
                let alerta: any={};
                if (planoTmp.recursosPlano[key][key2].backColor=='red'||planoTmp.recursosPlano[key][key2].backColor=='yellow'){
                  alerta.nombre=planoTmp.nombre+'--'+planoTmp.recursosPlano[key][key2].nombre;
                  alerta.backColor=planoTmp.recursosPlano[key][key2].backColor;
                  this.alertas.push(alerta);
                }

              }
            );
            console.log("444444444444444444");
            //console.log("tmp11111", tmp);
            console.log("PLANOTMPPPPPPPPP", planoTmp);


          });

          if ((this.selectedData != null) && (planoTmp.PKid == this.selectedData.PKid)) {
            console.log("actualizandoooooo selectedData", this.selectedData);
            this.selectedData = planoTmp;
          }


          return planoTmp;
        });


        console.table(this.planosWithChildren);

        this.nodes = this.transformToTree(this.planosWithChildren);

        console.log("json generadooooooooooooooooooo", JSON.stringify(this.nodes, null, 2));
        // this.tree.treeModel.update();
        this.reloadOpenPlano2();
      },
      error => console.log(error));



  }


  /////////////////////////////////////////////////////////////////////////////////////////



  reloadData2Simu() {
    let backColors: string[] = ["red", ""];
    let backColors2: string[] = ["green", "yellow", "red"];

    this.planos = this.PlanoService.getPlanoJson();
    this.planos.subscribe(
      (data: Planos[]) => {
        console.log("los datos", data);

        this.planosWithChildren = Object.keys(data).map((key: string) => {
          let planoTmp: PlanosWithChildren = new PlanosWithChildren();
          planoTmp.FKdominio = data[key].FKdominio;
          planoTmp.PKid = data[key].PKid;
          planoTmp.idPlanoPadre = data[key].idPlanoPadre;
          planoTmp.nombre = data[key].nombre;
          planoTmp.nombreFichero = data[key].nombreFichero;
          planoTmp.recursosPlano = data[key].recursosPlano;
          planoTmp.fichero = data[key].fichero;
          let randomIndex = Math.floor(Math.random() * backColors.length);
          let randomColor = backColors[randomIndex];
          planoTmp.backColor = randomColor;


          Object.keys(planoTmp.recursosPlano).map(key => {
            console.log("sidebar", key);
            console.log("sidebar ", planoTmp.recursosPlano[key]);

            Object.keys(planoTmp.recursosPlano[key]).map(key2 => {
              planoTmp.recursosPlano[key][key2];
              console.log("sidebar ", planoTmp.recursosPlano[key][key2]);

              let randomIndex2 = Math.floor(Math.random() * backColors2.length);
              let randomColor2 = backColors2[randomIndex2];
              planoTmp.recursosPlano[key][key2].backColor = randomColor2;
              if (planoTmp.recursosPlano[key][key2].backColor == "red") {
                this.mensajeAlerta.emit(planoTmp);
              }
              let alerta: any={};
              if (planoTmp.recursosPlano[key][key2].backColor=='red'||planoTmp.recursosPlano[key][key2].backColor=='yellow'){
                alerta.nombre=planoTmp.nombre+'--'+planoTmp.recursosPlano[key][key2].nombre;
                alerta.backColor=planoTmp.recursosPlano[key][key2].backColor;
                this.alertas.push(alerta);
              }

            }
            );
            console.log("444444444444444444");
            //console.log("tmp11111", tmp);
            console.log("PLANOTMPPPPPPPPP", planoTmp);


          });

          if ((this.selectedData != null) && (planoTmp.PKid == this.selectedData.PKid)) {
            console.log("actualizandoooooo selectedData", this.selectedData);
            this.selectedData = planoTmp;
          }


          return planoTmp;
        });


        console.table(this.planosWithChildren);

        this.nodes = this.transformToTree(this.planosWithChildren);

        console.log("json generadooooooooooooooooooo", JSON.stringify(this.nodes, null, 2));
        // this.tree.treeModel.update();
        this.reloadOpenPlano2();
      },
      error => console.log(error));



  }


  transformToTree(arr) {
    let nodes = {};
    return arr.filter(function (obj) {
      let id = obj["PKid"],
        parentId = obj["idPlanoPadre"];

      nodes[id] = _.defaults(obj, nodes[id], { children: [] });
      (parentId != -1) && (nodes[parentId] = (nodes[parentId] || { children: [] }))["children"].push(obj);

      return (parentId == -1);
    });
  }

  onUpdateData(treeComponent: TreeComponent, $event) {
    /*console.log ("onUpdateData", treeComponent, $event);
    treeComponent.treeModel.expandAll();*/
  }

  initializeWebSocketConnection() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/chat", (message) => {
        if (message.body) {
          $(".chat").append("<div class='message'>" + message.body + "</div>")
          console.log(message.body);
          let jsonData = JSON.parse(message.body);
          $("#node_" + jsonData.id).attr('class', jsonData.alerta);
        }
      });
    });
  }

  sendMessage(message) {
    this.stompClient.send("/app/send/message", {}, message);
    $('#input').val('');
    this.central.nativeElement.classList.remove('mostrar');
    this.central.nativeElement.classList.add('ocultar');
  }


  openPlano2(event, data) {
    console.log("openPlano2 de sidebar.component.ts:", event);
    this.selectedData = data;

    this.changePlano.emit({
      //flavorCode: eve.flavorCode,
      // planoUrl: data.nombreFichero
      planoUrl: data


    });
    console.log("openPlano2 de sidebar.component.ts----->planoURL:", data.nombreFichero);

  }

  reloadOpenPlano2() {
    console.log("reloadOpenPlano2 de sidebar.component.ts:");

    let planosWithChildrenObservable=of(this.planosWithChildren);
    this.changePlanoAlertas.emit({
      //flavorCode: eve.flavorCode,
      // planoUrl: data.nombreFichero
      planoUrl: this.selectedData,
      alertas: this.alertas

    });
    console.log("reloadOpenPlano2 de sidebar.component.ts----->planoURL:", this.selectedData);
  }






  openSidebar() {
    this.sidebar.nativeElement.classList.remove('ocultar');
    this.sidebar.nativeElement.classList.add('mostrar');


    console.log("evento:", event);
    console.log("valueSidebar", this.valueSidebar);
  }

  public anularAlarmas(event) {
    console.log("anularAlarmas de sidebar.component.ts:", event);
    this.nodes.map(node => {
      node.backColor = "";
      return node;
    });
  }




}
