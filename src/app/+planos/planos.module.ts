import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { PlaneListComponent } from './plane-list/plane-list.component';
import { HttpClientModule } from '@angular/common/http';
import { TreeModule } from 'angular-tree-component';
import { CommonModule } from '@angular/common';
import { AlertModule as MkAlertModule, BoxInfoModule as MkBoxInfoModule, BoxModule, DropdownModule } from 'angular-admin-lte';
// import { MatSliderModule } from '@angular/material/slider';


/* import {BrowserAnimationsModule} from '@angular/platform-browser/animations';*/
/* import { MatToolbarModule} from '@angular/material/toolbar';
import {MatSelectModule} from '@angular/material/select';

import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatListModule} from '@angular/material/list';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button'; */
import { ResizableModule } from 'angular-resizable-element';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {PopoverModule} from 'ngx-bootstrap/popover';
import {TabsModule} from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AlertTabComponent } from './alert-tab/alert-tab.component'
import {PlanosComponent} from './planos.component';
import {PlanosRoutingModule} from './planos-routing.module';
import {ToolComponent} from '../tool/tool.component';
import {ToolModule} from '../tool/tool.module';

import { NgFallimgModule } from 'ng-fallimg';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {PlanoDialogComponent} from "./plano-dialog/plano-dialog.component";
import { BtnGenericComponent } from './btn-generic/btn-generic.component';
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api';                 //api
import {ToastModule} from 'primeng/toast';
/* import {MatBadgeModule} from '@angular/material/badge'; */
import { from } from 'rxjs';
import { DropdownComponent } from 'library/angular-admin-lte/src/lib/dropdown/dropdown.component';




@NgModule({
  declarations: [

    PlaneListComponent,
    PlanosComponent,
    SidebarComponent,
    AlertTabComponent,
    PlanoDialogComponent,
    BtnGenericComponent,





  ],
  imports: [

    PlanosRoutingModule,

    // BrowserModule,
    //BrowserAnimationsModule,
    CommonModule,
    MkAlertModule,
    BoxModule,
    MkBoxInfoModule,

    ReactiveFormsModule,
    FormsModule,
    TreeModule.forRoot(),
    //HttpClientModule,
    //MatSlideToggleModule,
   
    
    ResizableModule,
    TooltipModule.forRoot(),
    TabsModule,
    BsDropdownModule,
    //MatButtonModule,
    //MatCheckboxModule,
    ModalModule.forRoot(),
    TreeModule.forRoot(),
    NgFallimgModule.forRoot({
      default: 'assets/img/blanco.png',

    }),/*
    SweetAlert2Module.forRoot(),*/
    SweetAlert2Module,
    PopoverModule,
    //MatToolbarModule,
    //MatCardModule,
    //MatInputModule,
    //MatListModule,
    // MatButtonModule,
    //MatIconModule,
    // MatCheckboxModule,
    // MatDialogModule,
    //MatFormFieldModule,
    FontAwesomeModule,
    //MatDialogModule,
    BsDatepickerModule,
    AccordionModule,
    //MatBadgeModule,
    //MatSelectModule,
    DropdownModule
   
    






  ],
  exports: [],
  entryComponents: [
    BtnGenericComponent
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class PlanosModule { }
