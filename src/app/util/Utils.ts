// import DeviceDetector from "device-detector-js";

import * as DeviceDetector from 'device-detector-js';

export default class Utils {
  public static isMobile() {
    const deviceDetector = new DeviceDetector();
    const userAgent = window.navigator.userAgent;
    const device = deviceDetector.parse(userAgent);
    let deviceType: string = device.device.type;
    if (deviceType == 'tablet' || deviceType == 'smartphone') {
      console.log("isMobile");
      return true;
    } else {
      console.log("NO isMobile");
      return false;
    }
  }
}
