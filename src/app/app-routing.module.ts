import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { AppComponent } from './app.component';

import { ToolComponent } from './tool/tool.component';
import { PlanosComponent } from './+planos/planos.component';
import { PlanosModule } from './+planos/planos.module';

const routes: Routes = [


  {
    path: '',
    loadChildren: './+planos/planos.module#PlanosModule',
    pathMatch: 'full',
    data: { title: 'Get Started' },
    /* children: [

      {
        path: 'tool',
        loadChildren: './tool/tool.module#ToolModule',
        pathMatch: 'full',
        
      },

    ] */
  }, {
    path: 'login',
    loadChildren: './+login/login.module#LoginModule',
    data: {
      customLayout: true
    }
  }, {
    path: 'register',
    loadChildren: './+register/register.module#RegisterModule',
    data: {
      customLayout: true
    }
  },
  {
    path: 'tool', 
    loadChildren:'./tool/app-tool.module#AppToolModule', 
    
    
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' }),


  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
