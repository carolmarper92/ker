import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.css']
})
export class NavmenuComponent implements OnInit {
  @Input() opcionMenu: string;
  title: string;

  constructor() { }

  ngOnInit(): void {
    this.titleChange(this.opcionMenu);
    console.log("estoy en navbar");
    console.log("estoy en menu imprimiento el titulo" + this.opcionMenu);
  }

  titleChange(opcion: string): void {
    console.log("dentro titltechange:"+opcion)

    if (opcion == "usuarios") {
      this.title = "Config. usuarios y llaves";

    } else if (opcion == "alertas") {
      this.title = "Config. usuarios y llaves";
    } else if (opcion == "informes") {
      this.title = "Informes";
    } else if (opcion == "test") {
      this.title = "Test";
    } else if (opcion == "infosis") {
      this.title = "Config. usuarios y llaves";
    } else if (opcion == "help") {
      this.title = "Ayuda";
    }
    console.log("dentro titltechange 2:"+this.title)
  }

}
