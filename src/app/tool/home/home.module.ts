import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ToolComponent } from '../tool.component';
import { InformeComponent } from './informe/informe.component';
import { TestComponent } from './test/test.component';
import { InfoComponent } from './info/info.component';
import { AdvancesettingComponent } from './advancesetting/advancesetting.component';
import { HelpComponent } from './help/help.component';

import { ToolModule } from '../tool.module';
import { NavmenuComponent } from './navmenu/navmenu.component';
import { SidebarhomeComponent } from './sidebarhome/sidebarhome.component';
import { TooluserComponent } from './tooluser/tooluser.component';
import { TooluserModule } from './tooluser/tooluser.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    HomeComponent,
    //TooluserComponent,
    
    InformeComponent,
    TestComponent,
    InfoComponent,
    AdvancesettingComponent,
    HelpComponent,
    NavmenuComponent,
    SidebarhomeComponent,
    
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    TooluserModule,
    
    
    
   
  ],exports:[
    HomeComponent
  ],schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
  
})
export class HomeModule { }
