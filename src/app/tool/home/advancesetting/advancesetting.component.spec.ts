import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancesettingComponent } from './advancesetting.component';

describe('AdvancesettingComponent', () => {
  let component: AdvancesettingComponent;
  let fixture: ComponentFixture<AdvancesettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvancesettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancesettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
