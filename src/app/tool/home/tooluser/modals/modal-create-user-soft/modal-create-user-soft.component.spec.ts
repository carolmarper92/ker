import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCreateUserSoftComponent } from './modal-create-user-soft.component';

describe('ModalCreateUserSoftComponent', () => {
  let component: ModalCreateUserSoftComponent;
  let fixture: ComponentFixture<ModalCreateUserSoftComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalCreateUserSoftComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCreateUserSoftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
