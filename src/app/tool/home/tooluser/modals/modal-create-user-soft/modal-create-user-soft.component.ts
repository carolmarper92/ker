import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-create-user-soft',
  templateUrl: './modal-create-user-soft.component.html',
  styleUrls: ['./modal-create-user-soft.component.scss']
})
export class ModalCreateUserSoftComponent implements OnInit {

  @Input() fromParent;

  constructor(public activeModal: NgbActiveModal ) { }

  ngOnInit() {
    console.log(this.fromParent);
    
  }

  //Cierre del modal. Envio de datos
  closeModal(sendData) {
    this.activeModal.close(sendData);
    console.log(sendData);
  }

}
