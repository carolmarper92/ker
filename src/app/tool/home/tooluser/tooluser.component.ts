import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { Observable } from 'rxjs';
import { Usuarioacceso } from './usuarioacceso';
import { UsuarioaccesoService } from './usuarioacceso.service';


@Component({
  selector: 'app-tooluser',
  templateUrl: './tooluser.component.html',
  styleUrls: ['./tooluser.component.css'],
})
export class TooluserComponent implements OnInit {
  rows = [];
  columns = [{ prop: 'PKFKid' },{ name: 'nombreUsrAcceso' },{ name: 'apellidos' },{ name: 'Apellido' }, { name: 'Código' }, { name: 'Dpto' }];
 
 
  usuarioacceso: Observable<Usuarioacceso[]>;

  constructor(private usuarioaccesoService: UsuarioaccesoService, private router: Router){}

  

  reloadData() {
    console.log("estoy en TOOLUSER");
    this.usuarioacceso = this.usuarioaccesoService.getAllUsuarioAcceso();
    this.usuarioacceso.subscribe(
      (data: Usuarioacceso[]) => {
        //this.rows= data[].;
        console.log("los datos",data);
      },
      error => console.log(error));
           
  }
  ngOnInit(): void {
    this.reloadData();
    console.log("111111estoy en tooluser")
  }
  

}
