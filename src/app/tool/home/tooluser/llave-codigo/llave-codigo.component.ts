import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';

import * as moment from 'moment';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DataTableDirective } from 'angular-datatables';
import { Recursoacceso } from '../recursoacceso';
import { RecursoAccesoService } from '../recursoacceso.service';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import { Usuarioacceso } from '../usuarioacceso';
import { ModalCreateUserSoftComponent } from '../modals/modal-create-user-soft/modal-create-user-soft.component';
import { UsuarioaccesoService } from '../usuarioacceso.service';
@Component({
  selector: 'app-llave-codigo',
  templateUrl: './llave-codigo.component.html',
  styleUrls: ['./llave-codigo.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class LlaveCodigoComponent implements OnInit {
  @ViewChild('formTabUserSoft')
  private formTabUserSoft: ElementRef;

  @ViewChild('deleteForm')
  private deleteForm: ElementRef;

  @ViewChild('resetForm')
  private resetForm: ElementRef;

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  recursoAcceso: Observable<Recursoacceso[]>;
  recursosAcceso: Recursoacceso[];

  usuarioAcceso: Observable<Usuarioacceso[]>;
  usuariosAceso: Usuarioacceso[];

  dtInstance: DataTables.Api;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
 
  endDate = moment();
  calendarOptions: any;
  //dtOptions: DataTables.Settings = {};

  //crear usuario usado en modal
  //usuarioAcceso:Usuarioacceso;

  formulario: FormGroup;
  rowdata: Recursoacceso = new Recursoacceso();
  submitted = false;
  nuevo = false;


  pruebita:Usuarioacceso;

  constructor(private recursoAccesoService: RecursoAccesoService, private UsuarioAccesoService: UsuarioaccesoService, private renderer: Renderer2,private modalService: NgbModal) {
    this.pruebita= new Usuarioacceso();
  }

  //Apertura del modal
  open() {
    //caracteristicas del modal. Le pasamos de parametro el componente modal
    const modalRef = this.modalService.open(ModalCreateUserSoftComponent,{
      scrollable:true,
      size:'lg'
    });

    //pasamos el objeto UsuarioAcceso vacio para rellenarlo en el modal y asi tenerlo en el 
    //componente padre para no tener que hacer llamada a BBDD
    /* modalRef.componentInstance.fromParent= this.usuarioAcceso = new Usuarioacceso();
    modalRef.result.then((result) => {
      console.log("en llave-codigo"+result.dni);
      console.log("en llave-codigo imprimo el objeto usuarioAcceso"+this.usuarioAcceso.dni);
      this.pruebita=result;
    }, (reason) => {
    }); */

    console.log("esto es la variable pruebita"+this.pruebita);

  }


  reloadData() {

    console.log("estoy en LLAVES-CODIGOS vamos a ver los datos");

    this.recursoAcceso = this.recursoAccesoService.getAllRecursoAcceso();
    this.usuarioAcceso = this.UsuarioAccesoService.getAllUsuarioAcceso();

    this.recursoAcceso.subscribe(
      (data: Recursoacceso[]) => {
        this.recursosAcceso = data;
        this.dtTrigger.next();
        console.log("los datos1 ", data);
        console.log("los datos", this.recursosAcceso);
        /* for (let i = 0; i < this.recursosAcceso.length; i++) {

          console.log("los datos subscribe", this.recursosAcceso[i].pkfkid);
        } */
        console.log('ultimo valor array ' + this.recursosAcceso[this.recursosAcceso.length - 1].pkfkid)

      },
      error => console.log(error));


  }
  ngOnInit(): void {
    this.reloadData();
    console.log("estoy en LLAVES-CODIGOS");
    //table(); //llamada a la funcion de la tabla del script

    this.dtOptions = {
      data: this.recursosAcceso,
      columns: [{ data: 'pkfkid' }, { data: 'codigo' }, { data: 'tipo' }, { data: 'idUsuarioAcceso', "visible": false }, { data: 'estadoTemp', "visible": false }, { data: 'validaDesde', "visible": false }, { data: 'validaHasta' }, { data: 'bajaDesde', "visible": false }, { data: 'bajaHasta', "visible": false }, { data: 'idPlantilla', "visible": false }, { data: 'idGrupoUsuariosAcceso', "visible": false }, { data: 'aviso', "visible": false }],
      select: true,
      responsive: true,
      lengthChange: false,
      pageLength:5,
      autoWidth: false,
      paging: true,
      searching: true,
      ordering: true,
      info: true,
      scrollX: true,
      pagingType: 'first_last_numbers',
      deferRender: true,

      language: {
        emptyTable: 'Tabla vacia',
        zeroRecords: 'No hay coincidencias',
        lengthMenu: 'Mostrar _MENU_ elementos',
        search: 'Buscar:',
        info: 'De _START_ a _END_ de _TOTAL_ elementos',
        infoEmpty: 'De 0 a 0 de 0 elementos',
        infoFiltered: '(filtrados de _MAX_ elementos totales)',
        paginate: {
          first: '<<',
          last: '>>',
          next: '>',
          previous: '<'
        },
        buttons: {
          print: "PDF_",
          copy: "Copiar",
          colvis: "Visibilidad",
          collection: "Colección",
          colvisRestore: "Restaurar visibilidad",
          copyKeys: "Presione ctrl o u2318 + C para copiar los datos de la tabla al portapapeles del sistema. <br \/> <br \/> Para cancelar, haga clic en este mensaje o presione escape.",
          copySuccess: {
            "1": "Copiada 1 fila al portapapeles",
            "_": "Copiadas %d fila al portapapeles"
          },
        },
        select: {

          rows: {
            "1": "1 fila seleccionada",
            "_": "%d filas seleccionadas"
          }
        },
      },
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [

        'colvis',
        'copy',
        'print',
        'excel',
        {
          text: 'Some button',
          key: '1',
          action: function (e, dt, node, config) {
            alert('Button activated');
          }
        }
      ],
      rowCallback: (row: Node, data: Recursoacceso[] | object, index: number) => {
        const self = this;
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        // Note: In newer jQuery v3 versions, `unbind` and `bind` are
        // deprecated in favor of `off` and `on`
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.clickRow(data);

        });

        return row;
      },

    };

/*     this.calendarOptions = {
      format: 'DD/MM/YYYY hh:mm',
      buttons: { showClear: true },
      icons: { clear: 'fa fa-trash' },

    };
    console.log("fecha hoy" + this.endDate.format('DD/MM/YYYY HH:MM'))
    this.calendarOptions.maxDate = this.endDate;
 */
  }

  //alerta del boton delete dentro del form
  alertWithDelete(pkfkid) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {

        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger mr-5',
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: '¿Seguro que quieres eliminar el usuario ' + pkfkid + ' ?',
      text: 'No podrás recuperar a este usuario!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, borralo!',
      cancelButtonText: 'No, cancela!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        Swal.fire(
          'Borrado!',
          'El usuario ' + pkfkid + ' ha sido borrado correctamente.',
          'success'
        );
        this.deleteUsuarioAcceso(pkfkid);
        this.rerender();

        this.renderer.addClass(this.formTabUserSoft.nativeElement, "d-none");
        this.rowdata = new Recursoacceso();

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'No olvides guardar tus cambios :)',
          'error'
        )
      }
    })
  }
  //alerta del boton guardar dentro del form
  alertWithSave(): void {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Usuario de acceso guardado con exito',
      showConfirmButton: false,
      timer: 1500
    })
  }


  //Acciones para realizar al hacer click en las filas de DataTables
  clickRow(data: any): void {

    /* console.log("recibir datos" + data.dni);
    console.log("recibir datos" + data.pkfkid);
    console.log("recibir datos fechaM" + data.fechaModificacion + "---");
 */
    this.rowdata = data;
    this.renderer.removeClass(this.formTabUserSoft.nativeElement, "d-none"); //mostrar formulario
    this.renderer.removeClass(this.deleteForm.nativeElement, "invisible"); // mostrar boton delete
    this.renderer.addClass(this.resetForm.nativeElement, "invisible"); //ocultar boton reset


    /* console.log("recibir datos2" + this.rowdata.apellidos);
    console.log("recibir datos2" + this.rowdata.fechaNacimiento);
    console.log("recibir datos2" + this.rowdata.pkfkid);
    console.log("recibir datos2" + this.rowdata.fechaModificacion);
    console.log("departamento" + this.rowdata.fkidDept); */
  }

  //Ocultar formulario (No mantiene un espacio reservado para el)
  hideForm(): void {
    this.renderer.addClass(this.formTabUserSoft.nativeElement, "d-none");

  }

  editForm(): void {
    //this.renderer.removeClass(this.resetForm.nativeElement, "invisible"); // mostrar boton reset
    //this.nuevo=false;
    console.log("probando edit");

  }

  //Nuevo usuario. reseteo de formulario, dar visibilidad al formulario, ocultar btn borrar
  userNew(): void {
    this.nuevo = true;
    console.log("variable nuevo" + this.nuevo);
    this.rowdata = new Recursoacceso();
    this.renderer.removeClass(this.formTabUserSoft.nativeElement, "d-none"); //mostrar formulario
    this.renderer.addClass(this.deleteForm.nativeElement, "invisible"); //ocultar boton delete
    this.renderer.removeClass(this.resetForm.nativeElement, "invisible"); //mostrar boton reset
  }

  deleteUsuarioAcceso(pkfkid: number): void {
    this.recursoAccesoService.deleterecursoAcceso(pkfkid)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  save() {
    //this.rowdata.pkfkid=this.recursosAcceso[this.recursosAcceso.length -1].pkfkid + 1; //sumamos 1 al ultimo pkfkid de la tabla
    //console.log(this.rowdata.pkfkid + " " + " " + this.rowdata.tipoUsuario);
    //this.rowdata.nombreUsrAcceso = 'aaaa';
    console.log("variable nuevo" + this.nuevo);

    if (this.nuevo) {
      this.recursoAccesoService.createRecursoAcceso(this.rowdata)
        .subscribe(data => console.log(data), error => console.log(error));
      this.rowdata = new Recursoacceso();
      console.log(this.rowdata);


    } else {
      console.log("Estoy guardando lo editado" + this.rowdata.pkfkid + "  " + this.rowdata);

      this.recursoAccesoService.updaterecursoAcceso(this.rowdata.pkfkid, this.rowdata)
        .subscribe(data => console.log(data),
          error => console.log(error));
      this.nuevo = false;
    }
    this.alertWithSave();



  }
  onSubmit() {
    this.submitted = true;
    this.save();
  }



  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();

  }


  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      //console.log("dtinstance: " + dtInstance)
      // Destroy the table first
      dtInstance.destroy();
      
      //this.recursoAcceso;
      // Call the dtTrigger to rerender again
      
      this.dtTrigger.next();
      
      
    });
  }






}
