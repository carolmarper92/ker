import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LlaveCodigoComponent } from './llave-codigo.component';

describe('LlaveCodigoComponent', () => {
  let component: LlaveCodigoComponent;
  let fixture: ComponentFixture<LlaveCodigoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LlaveCodigoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LlaveCodigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
