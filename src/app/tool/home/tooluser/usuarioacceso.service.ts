import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioaccesoService {

  private baseUrl = '/toolback/tool/home/usuarios/usersoft';

  constructor(private http: HttpClient) { }

  getAllUsuarioAcceso(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
    
  }
  getUsuarioacceso(pkfkid: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${pkfkid}`);
  }

  createUsuarioAcceso(usuarioAcceso: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, usuarioAcceso);
  }

  updateUsuarioAcceso(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteUsuarioacceso(pkfkid: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${pkfkid}`, { responseType: 'text' });
  }

  
  
}
