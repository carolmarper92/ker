import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TooluserComponent } from './tooluser.component';

describe('TooluserComponent', () => {
  let component: TooluserComponent;
  let fixture: ComponentFixture<TooluserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TooluserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TooluserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
