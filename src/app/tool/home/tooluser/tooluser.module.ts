import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooluserRoutingModule } from './tooluser-routing.module';

import { FormsModule, FormsModule as ngFormsModule } from '@angular/forms';
import { UserSoftComponent } from './user-soft/user-soft.component';
import { TooluserComponent } from './tooluser.component';
import { DataTablesModule } from 'angular-datatables';
import { NgTempusdominusBootstrapModule } from 'ngx-tempusdominus-bootstrap';
import { LlaveCodigoComponent } from './llave-codigo/llave-codigo.component';
import { ModalCreateUserSoftComponent } from './modals/modal-create-user-soft/modal-create-user-soft.component';



@NgModule({
  declarations: [
    UserSoftComponent,
    TooluserComponent,
    LlaveCodigoComponent,
    ModalCreateUserSoftComponent
    
  ],
  imports: [
    CommonModule,
    TooluserRoutingModule,
    ngFormsModule,
    DataTablesModule,
    FormsModule,
    NgTempusdominusBootstrapModule
   
  ],exports:[
    UserSoftComponent,TooluserComponent,LlaveCodigoComponent

  ],
})  
export class TooluserModule { }
