import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecursoAccesoService {

  private baseUrl = '/toolback/tool/home/usuarios/codekey';

  constructor(private http: HttpClient) { }

  getAllRecursoAcceso(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
    
  }
  getRecursoAcceso(pkfkid: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${pkfkid}`);
  }

  createRecursoAcceso(recursoAcceso: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, recursoAcceso);
  }

  updaterecursoAcceso(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleterecursoAcceso(pkfkid: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${pkfkid}`, { responseType: 'text' });
  }

  
  
}
