import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSoftComponent } from './user-soft.component';

describe('UserSoftComponent', () => {
  let component: UserSoftComponent;
  let fixture: ComponentFixture<UserSoftComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserSoftComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSoftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
