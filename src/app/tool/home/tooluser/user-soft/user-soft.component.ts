import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { Usuarioacceso } from '../usuarioacceso';
import { UsuarioaccesoService } from '../usuarioacceso.service';
import * as moment from 'moment';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DataTableDirective } from 'angular-datatables';

declare const table: any;
@Component({
  selector: 'app-user-soft',
  templateUrl: './user-soft.component.html',
  styleUrls: ['./user-soft.component.css'],
})
export class UserSoftComponent implements OnInit, OnDestroy {

  @ViewChild('formTabUserSoft')
  private formTabUserSoft: ElementRef;

  @ViewChild('deleteForm')
  private deleteForm: ElementRef;

  @ViewChild('resetForm')
  private resetForm: ElementRef;

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  usuarioacceso: Observable<Usuarioacceso[]>;
  usuariosAcceso: Usuarioacceso[];

  dtInstance: DataTables.Api;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
 
  endDate = moment();
  calendarOptions: any;
  //dtOptions: DataTables.Settings = {};

  formulario: FormGroup;
  rowdata: Usuarioacceso = new Usuarioacceso();
  submitted = false;
  nuevo = false;

  constructor(private usuarioaccesoService: UsuarioaccesoService, private renderer: Renderer2) {

  }


  reloadData() {

    console.log("estoy en USER-SOFT vamos a ver los datos");

    this.usuarioacceso = this.usuarioaccesoService.getAllUsuarioAcceso();
    /*this.usuarioaccesoService.getAllUsuarioAcceso().subscribe(data =>{
      this.usuario=data;
    });*/
    this.usuarioacceso.subscribe(
      (data: Usuarioacceso[]) => {
        this.usuariosAcceso = data;
        this.dtTrigger.next();
        console.log("los datos1 ", data);
        console.log("los datos", this.usuariosAcceso);
        for (let i = 0; i < this.usuariosAcceso.length; i++) {

          console.log("los datos subscribe", this.usuariosAcceso[i].pkfkid + " " + this.usuariosAcceso[i].apellidos + "con fecha nacimiento" + this.usuariosAcceso[i].fechaNacimiento + "con fecha mod" + this.usuariosAcceso[i].fechaModificacion + "departamento" + this.usuariosAcceso[i].fkidDept);
        }
        //console.log("los datos", this.usuariosAcceso[0].apellidos + "con fecha alta" + this.usuariosAcceso[0].fechaAltaUsuario);
        console.log('ultimo valor array ' + this.usuariosAcceso[this.usuariosAcceso.length - 1].pkfkid)

      },
      error => console.log(error));

    /*        this.usuarioaccesoService.getAllUsuarioAcceso().subscribe(
            data =>{
              this.usuarioacceso=data;
              console.log(data);
            }
          );  */



  }
  ngOnInit(): void {
    this.reloadData();
    console.log("estoy en USER-SOFT");
    //table(); //llamada a la funcion de la tabla del script



    this.dtOptions = {
      data: this.usuariosAcceso,
      columns: [{ data: 'pkfkid' }, { data: 'dni' }, { data: 'apellidos' }, { data: 'tipoUsuario' }, { data: 'fechaNacimiento', "visible": false }, { data: 'domicilio', "visible": false }, { data: 'poblacion', "visible": false }, { data: 'provincia', "visible": false }, { data: 'codigoPostal', "visible": false }, { data: 'telefono1' }, { data: 'telefono2', "visible": false }, { data: 'extension1' }, { data: 'extension2', "visible": false }, { data: 'fechaModificacion', "visible": false }, { data: 'fkidDept', "visible": false }, { data: 'empresa' }, { data: 'idrecinto', "visible": false }, { data: 'matriculaVehiculo', "visible": false }, { data: 'nombreUsrAcceso', "visible": false }, { data: 'fechaAltaUsuario', "visible": false }, { data: 'codigo' }, { data: 'confidencial', "visible": false }, { data: 'observaciones', "visible": false }],
      select: true,
      responsive: true,
      lengthChange: false,
      autoWidth: false,
      paging: true,
      searching: true,
      ordering: true,
      info: true,
      scrollX: true,
      pagingType: 'first_last_numbers',
      deferRender: true,

      language: {
        emptyTable: 'Tabla vacia',
        zeroRecords: 'No hay coincidencias',
        lengthMenu: 'Mostrar _MENU_ elementos',
        search: 'Buscar:',
        info: 'De _START_ a _END_ de _TOTAL_ elementos',
        infoEmpty: 'De 0 a 0 de 0 elementos',
        infoFiltered: '(filtrados de _MAX_ elementos totales)',
        paginate: {
          first: '<<',
          last: '>>',
          next: '>',
          previous: '<'
        },
        buttons: {
          print: "PDF_",
          copy: "Copiar",
          colvis: "Visibilidad",
          collection: "Colección",
          colvisRestore: "Restaurar visibilidad",
          copyKeys: "Presione ctrl o u2318 + C para copiar los datos de la tabla al portapapeles del sistema. <br \/> <br \/> Para cancelar, haga clic en este mensaje o presione escape.",
          copySuccess: {
            "1": "Copiada 1 fila al portapapeles",
            "_": "Copiadas %d fila al portapapeles"
          },
        },
        select: {

          rows: {
            "1": "1 fila seleccionada",
            "_": "%d filas seleccionadas"
          }
        },
      },
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [

        'colvis',
        'copy',
        'print',
        'excel',
        {
          text: 'Some button',
          key: '1',
          action: function (e, dt, node, config) {
            alert('Button activated');
          }
        }
      ],
      rowCallback: (row: Node, data: Usuarioacceso[] | object, index: number) => {
        const self = this;
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        // Note: In newer jQuery v3 versions, `unbind` and `bind` are
        // deprecated in favor of `off` and `on`
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.clickRow(data);

        });

        return row;
      },

    };

    this.calendarOptions = {
      format: 'DD/MM/YYYY hh:mm',
      buttons: { showClear: true },
      icons: { clear: 'fa fa-trash' },

    };
    console.log("fecha hoy" + this.endDate.format('DD/MM/YYYY HH:MM'))
    this.calendarOptions.maxDate = this.endDate;

  }

  //alerta del boton delete dentro del form
  alertWithDelete(pkfkid) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {

        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger mr-5',
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: '¿Seguro que quieres eliminar el usuario ' + pkfkid + ' ?',
      text: 'No podrás recuperar a este usuario!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, borralo!',
      cancelButtonText: 'No, cancela!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        Swal.fire(
          'Borrado!',
          'El usuario ' + pkfkid + ' ha sido borrado correctamente.',
          'success'
        );
        this.deleteUsuarioAcceso(pkfkid);
        this.rerender();

        this.renderer.addClass(this.formTabUserSoft.nativeElement, "d-none");
        this.rowdata = new Usuarioacceso();

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'No olvides guardar tus cambios :)',
          'error'
        )
      }
    })
  }
  //alerta del boton guardar dentro del form
  alertWithSave(): void {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Usuario de acceso guardado con exito',
      showConfirmButton: false,
      timer: 1500
    })
  }


  //Acciones para realizar al hacer click en las filas de DataTables
  clickRow(data: any): void {

    /* console.log("recibir datos" + data.dni);
    console.log("recibir datos" + data.pkfkid);
    console.log("recibir datos fechaM" + data.fechaModificacion + "---");
 */
    this.rowdata = data;
    this.renderer.removeClass(this.formTabUserSoft.nativeElement, "d-none"); //mostrar formulario
    this.renderer.removeClass(this.deleteForm.nativeElement, "invisible"); // mostrar boton delete
    this.renderer.addClass(this.resetForm.nativeElement, "invisible"); //ocultar boton reset


    /* console.log("recibir datos2" + this.rowdata.apellidos);
    console.log("recibir datos2" + this.rowdata.fechaNacimiento);
    console.log("recibir datos2" + this.rowdata.pkfkid);
    console.log("recibir datos2" + this.rowdata.fechaModificacion);
    console.log("departamento" + this.rowdata.fkidDept); */
  }

  //Ocultar formulario (No mantiene un espacio reservado para el)
  hideForm(): void {
    this.renderer.addClass(this.formTabUserSoft.nativeElement, "d-none");

  }

  editForm(): void {
    //this.renderer.removeClass(this.resetForm.nativeElement, "invisible"); // mostrar boton reset
    //this.nuevo=false;
    console.log("probando edit");

  }

  //Nuevo usuario. reseteo de formulario, dar visibilidad al formulario, ocultar btn borrar
  userNew(): void {
    this.nuevo = true;
    console.log("variable nuevo" + this.nuevo);
    this.rowdata = new Usuarioacceso();
    this.renderer.removeClass(this.formTabUserSoft.nativeElement, "d-none"); //mostrar formulario
    this.renderer.addClass(this.deleteForm.nativeElement, "invisible"); //ocultar boton delete
    this.renderer.removeClass(this.resetForm.nativeElement, "invisible"); //mostrar boton reset
  }

  deleteUsuarioAcceso(pkfkid: number): void {
    this.usuarioaccesoService.deleteUsuarioacceso(pkfkid)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  save() {
    //this.rowdata.pkfkid=this.usuariosAcceso[this.usuariosAcceso.length -1].pkfkid + 1; //sumamos 1 al ultimo pkfkid de la tabla
    console.log(this.rowdata.pkfkid + " " + " " + this.rowdata.tipoUsuario);
    this.rowdata.nombreUsrAcceso = 'aaaa';
    console.log("variable nuevo" + this.nuevo);

    if (this.nuevo) {
      this.usuarioaccesoService.createUsuarioAcceso(this.rowdata)
        .subscribe(data => console.log(data), error => console.log(error));
      this.rowdata = new Usuarioacceso();
      console.log(this.rowdata);


    } else {
      console.log("Estoy guardando lo editado" + this.rowdata.pkfkid + "  " + this.rowdata);

      this.usuarioaccesoService.updateUsuarioAcceso(this.rowdata.pkfkid, this.rowdata)
        .subscribe(data => console.log(data),
          error => console.log(error));
      this.nuevo = false;
    }
    this.alertWithSave();



  }
  onSubmit() {
    this.submitted = true;
    this.save();
  }



  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();

  }


  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      //console.log("dtinstance: " + dtInstance)
      // Destroy the table first
      dtInstance.destroy();
      
      //this.usuarioacceso;
      // Call the dtTrigger to rerender again
      
      this.dtTrigger.next();
      
      
    });
  }







}
