import { HttpParams } from '@angular/common/http';
import { ChangeDetectionStrategy } from '@angular/compiler/src/core';
import { FnParam } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, OutletContext, Router, Params, ParamMap, NavigationEnd, ActivationEnd } from '@angular/router';
import { NavItem } from './nav-item';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  navItems: NavItem[] = [
    {
      displayName: 'Usuario Software',
      route: 'usersoft',

    },
    {
      displayName: 'Usuario Tienda',
      route: 'usershop',

    },
    {
      displayName: 'Llave Codigo',
      route: 'codekey',

    },

  ]
  url: { opcion: string };
  opcion: string;


  constructor(private router: Router, private activatedRouter: ActivatedRoute) {
    //router.events.subscribe((url: any) => console.log(url));
    console.log("Pruebita: " + router.url[3]);
    console.log("array de estados" + activatedRouter.snapshot.url)  // array of states
    //console.log(activatedRouter.snapshot.url[0].path)

    console.log(router.url[0]);  // to print only path eg:"/login"
  }
  ngOnInit(): void {

    console.log('estoy en home');

    /* this.url = {
      opcion: this.activatedRouter.snapshot.params['usuarios']
    };
    this.activatedRouter.params.subscribe(
      (updatedParams) => {
        this.opcion = updatedParams['usuarios'];
        
      }
    );
    
    console.log("hasta el pijo ya: "+this.opcion);
  
    this.opcion=this.activatedRouter.params['usuarios'];
    console.log('this.opcion: '+this.opcion);
  
    this.activatedRouter.params.subscribe(
      (params: Params) => { 
        this.url = params.opcion;
        console.log(this.url);
  
      }
    ); */



    this.activatedRouter.paramMap.subscribe((parametros: ParamMap) => {
      this.opcion = parametros.get("usuarios");
      console.log('opcion' + this.opcion);

    })

    for (let elemento of this.navItems) {
      console.log('estoy en home imprimiendo: ' + elemento.displayName);
    }

  }



}
