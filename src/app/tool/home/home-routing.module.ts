import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { TooluserComponent } from './tooluser/tooluser.component';
import { TooluserModule } from './tooluser/tooluser.module';
import { UserSoftComponent } from './tooluser/user-soft/user-soft.component';
import { LlaveCodigoComponent } from './tooluser/llave-codigo/llave-codigo.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,

    children: [
      {
        path: ':usuarios',
        loadChildren: './tooluser/tooluser.module#TooluserModule',
        component: HomeComponent,
        //outlet: 'sidebar',

        children: [
          {
            path: 'usersoft',
            component: UserSoftComponent
          },
          {
            path: 'codekey',
            component: LlaveCodigoComponent
          },

        ]


      },



    ],


  },





];


@NgModule({

  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class HomeRoutingModule { }
