import { Component, NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { ToolComponent } from './tool.component';
import {AppToolComponent} from './app-tool.component'
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {
    path: '', component: AppToolComponent, 
    
    children:[
      {
        path:'',
        component:ToolComponent
      },
      {
        path:'home',
        loadChildren:'./home/home.module#HomeModule' 
      }

    ]
  },



];


@NgModule({

  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class AppToolRoutingModule { }
