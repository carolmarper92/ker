import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {AppToolRoutingModule} from './app-tool-routing.module';
import {AppToolComponent} from './app-tool.component';

import {HomeComponent} from './home/home.component';


import { ToolComponent } from './tool.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HomeModule } from './home/home.module';
import {ToolModule} from "./tool.module";



@NgModule({
  declarations: [
    AppToolComponent,
    ToolComponent,




  ],
  imports: [
    CommonModule,
    AppToolRoutingModule,
    HomeModule








  ],
  exports: [AppToolComponent],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]

})
export class AppToolModule { }
