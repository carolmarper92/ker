import { Component, NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { ToolComponent } from './tool.component';
import {HomeComponent} from './home/home.component';


const routes: Routes = [
  {
    path: '', component: ToolComponent, pathMatch: 'full',
    //rutas para los distintos menuses
    // path: 'usuarios', component: UsuariosToolComponent, pathMatch: 'full',

    // path: 'alertas', component: AlertasToolComponent, pathMatch: 'full',
    
  },



];


@NgModule({

  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class ToolRoutingModule { }
