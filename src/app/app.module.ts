import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { adminLteConf } from './admin-lte.conf';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { LayoutModule } from 'angular-admin-lte';
import { AppComponent } from './app.component';
import { LoadingPageModule, MaterialBarModule } from 'angular-loading-page';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {RouterModule} from '@angular/router';
import {MatDialogModule} from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { createPopper } from '@popperjs/core';

import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons';
import { faStar as fasStar } from '@fortawesome/free-solid-svg-icons';
import { faMap as farMap } from '@fortawesome/free-regular-svg-icons';
import { faMap as fasMap } from '@fortawesome/free-solid-svg-icons';
import { faBell as farBell } from '@fortawesome/free-regular-svg-icons';
import { faBell as fasBell } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle as fasInfoCircle } from '@fortawesome/free-solid-svg-icons';

//ngx-bootstrap
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PopoverModule  } from 'ngx-bootstrap/popover';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ToastNoAnimationModule, ToastrModule } from 'ngx-toastr';

//components
import { TreeComponent, TreeNode, TreeModel, TREE_ACTIONS, KEYS, IActionMapping, ITreeOptions, TreeModule } from 'angular-tree-component';
import {PlanosComponent} from './+planos/planos.component'
import {ToolModule} from './tool/tool.module';
import { PlanosModule } from './+planos/planos.module';

//primeng
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api';                 //api
import {ToastModule} from 'primeng/toast';
import { from } from 'rxjs';
import { ToolComponent } from './tool/tool.component';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule } from '@angular/forms';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';






@NgModule({
  imports: [
    
    CommonModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,  
    CoreModule,
    LayoutModule.forRoot(adminLteConf),
    LoadingPageModule, 
    MaterialBarModule, 
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    LoadingPageModule,
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    FontAwesomeModule,
    TreeModule.forRoot(),
    CollapseModule.forRoot(),
    SweetAlert2Module.forRoot(),
    PopoverModule.forRoot(),
    AccordionModule,
    ToastModule,
    RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
    MatDialogModule,
    ToastrModule.forRoot({
      countDuplicates: true,
      
    }),
    ToastNoAnimationModule.forRoot(),
    DataTablesModule,
    NgbModule

    
    
    
    
   
    
    







  ],
  declarations: [
    AppComponent,
    
    
    


  ],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  exports: [RouterModule ],
  
})
export class AppModule {
  constructor() {
    
  }
 }
