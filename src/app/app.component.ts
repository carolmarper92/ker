import { Component, OnInit, TemplateRef, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { LayoutService } from 'angular-admin-lte';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { faBars, faHome, faUser, faFirstAid, faInfo, faCaretDown,faUserAstronaut  } from '@fortawesome/free-solid-svg-icons';
import {faMap} from '@fortawesome/free-regular-svg-icons'
import { PlaneListComponent } from './+planos/plane-list/plane-list.component';
import { TreeComponent, TreeNode, TreeModel, TREE_ACTIONS, KEYS, IActionMapping, ITreeOptions } from 'angular-tree-component';
import { trigger, state, style, animate, transition, } from '@angular/animations';
import { SidebarComponent } from './+planos/sidebar/sidebar.component';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [{ provide: BsDropdownConfig, useValue: { isAnimated: true, autoClose: true } }],
  animations: [

    trigger('openClose', [
      state('true',

        style({
          transform: 'translateX(0%)'
        }),
        
      ),
      state(
        'false',

        style({
          transform: 'translateX(-100%)'
          
        })

      ),
      transition('false <=> true', animate('500ms'))


    ])
  ]


})
export class AppComponent implements OnInit {
  public customLayout: boolean;
  fondo: string = "blanco.png";
  modalRef: BsModalRef;

//icons
  faBars = faBars;
  faMap = faMap;
  faHome = faHome;
  faUser = faUser;
  faFirstAid = faFirstAid;
  faInfo = faInfo;
  faCaretDown = faCaretDown;
  faUserAstronaut = faUserAstronaut;

  // faSitemap = faSitemap;
  sidebarhide: boolean = true;


  @ViewChild("principal")
  private principal: ElementRef;
  @ViewChild(PlaneListComponent)
  private employeelist: PlaneListComponent;
  @ViewChild(SidebarComponent)
  private sideBar: SidebarComponent;

 
  constructor(
    private layoutService: LayoutService, private modalService: BsModalService, private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.layoutService.isCustomLayout.subscribe((value: boolean) => {
      this.customLayout = value;
    });
  }

  mostrarSidebar(): void {
    this.sidebarhide = !this.sidebarhide;
    console.log("sidebarhide",this.sidebarhide);
          
    //this.renderer.removeClass(this.principal.nativeElement,'col-md-10 col-sm-10 col-xs-12');
    if (!this.sidebarhide) {
      
       //this.renderer.removeClass(this.principal.nativeElement, 'princi'); //eliminar class
       
       this.renderer.removeClass(this.principal.nativeElement, 'toggeled-sidebar'); //tamaño pantallas pequeñas

    } else {
     
     
      this.renderer.addClass(this.principal.nativeElement, 'toggeled-sidebar'); //tamaño pantallas pequeñas
      
      

    }
    console.log("sidebarhide:", this.sidebarhide);


  }


  //PopUp ayuda-leyenda de colores
  openModal(template1: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template1, { class: 'modal-sm' });
  }
  //PopUp ayuda-Acerca de Argos
  openModal2(template2: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template2, { class: 'modal-sm' });
  }
  //PopUp ayuda-Acerca de Isis
  openModal3(template3: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template3, { class: '' });
  }


  onChangePlano(event) {
    console.log("onChangePlano de app.component.ts:", event);

    this.employeelist.changePlano(event);

  }


  

onAnularAlarmas(event){
  console.log("onAnularAlarmas de app.componet.ts",event);

  this.employeelist.anularAlarmas(event);
  this.sideBar.anularAlarmas(event);
}








}
